FROM openjdk:8-jre-alpine
EXPOSE 8080
COPY target/large-array-api-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -Xmx2G -jar app.jar"]