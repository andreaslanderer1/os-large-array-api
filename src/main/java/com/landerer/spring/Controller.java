package com.landerer.spring;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Random;

@RestController
@RequestMapping("/numbers")
public class Controller {

    private final int[] NUMBERS = new int[250_000_000];

    public Controller() {
        final Random r = new Random();
        int length = NUMBERS.length;
        for (int i = 0; i < length; i++) {
            NUMBERS[i] = r.nextInt();
        }
    }

    @GetMapping
    public ResponseEntity<int[]> numbers(@RequestParam("start") int start,
                                         @RequestParam("max") int max) {
        int end = (start + max) < NUMBERS.length ? (start + max) : NUMBERS.length;
        int[] subArray = Arrays.copyOfRange(NUMBERS, start, end);
        return ResponseEntity.ok(subArray);
    }
}
